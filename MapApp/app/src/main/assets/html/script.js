function goToRoom(room) {
    var coordsString = $("area[title='" + room + "']").attr("coords");
    var coords = coordsString.split(",");
    highlight(coords);
    $("area[title='" + room + "']").focus();
    window.scrollTo(centerXIfPossible(coords), centerYIfPossible(coords));
}

function centerXIfPossible(coords) {
    var x2 = coords[0] - window.innerWidth/2;
    return x2 >= 0 ? x2 : coords[0];
}

function centerYIfPossible(coords) {
    var y2 = coords[1] - window.innerHeight/2;
    return y2 >= 0 ? y2 : coords[1];
}

function highlight(coords) {
    var top = coords[1] - 20;
    var left = coords[0] - 20;
    $(".hover").remove();
    $("body").append("<div class='hover' style='top:" + top + "px;left:" + left + "px;'></div>");
}

///////////////////////////////////////////////

function goToRooms(location, destination) {
    var coordsString = $("area[title='" + location + "']").attr("coords");
    var coords = coordsString.split(",");
    highlight(coords);

    var coordsString2 = $("area[title='" + destination + "']").attr("coords");
    var coords2 = coordsString2.split(",");
    highlightDest(coords2);

    var coords3 = [Math.abs(coords[0] - coords2[0])/2 + Math.min(coords[0], coords2[0]), Math.abs(coords[1] - coords2[1])/2 + Math.min(coords[1], coords2[1])];
    window.scrollTo(centerXIfPossible(coords3), centerYIfPossible(coords3));
}

function highlightDest(coords) {
    var top = coords[1] - 20;
    var left = coords[0] - 20;
    $(".dest").remove();
    $("body").append("<div class='dest' style='top:" + top + "px;left:" + left + "px;'></div>");
}


function goToRoomsDest(destination) {
    var coordsString = $("area[title='" + destination + "']").attr("coords");
    var coords = coordsString.split(",");
    highlightDest(coords);
    window.scrollTo(centerXIfPossible(coords), centerYIfPossible(coords));
}
