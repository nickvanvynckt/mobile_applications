package mobile.mapapp;

import android.os.AsyncTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by nickvanvynckt on 23/01/17.
 */

class AsyncFetchData extends AsyncTask<String, List, Map<String, List<String>>> {

    public AsyncFetchData(){}

    @Override
    protected Map<String, List<String>> doInBackground(String... params) {
        Map<String, List<String>> data = new HashMap<>();
        try {
            URL url = new URL("https://mapapp.bertvandormael.be/macs");
            HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();
            InputStream in = new BufferedInputStream(urlConnection.getInputStream());
            BufferedReader reader = new BufferedReader(new InputStreamReader(in));
            String JSONstring = reader.readLine();
            JSONArray jsonArray = new JSONArray(JSONstring);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.getString("name");
                String acc_ps = obj.getString("acc_ps");
                List<String> acc_ps_list = Arrays.asList(acc_ps.split("\\s*,\\s*"));
                data.put(name, acc_ps_list);
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data;
    }

    @Override
    protected void onPostExecute(Map<String, List<String>> result){
    }
}
