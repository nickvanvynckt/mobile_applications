package mobile.mapapp;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by drumb on 26/01/2017.
 */

public class DestMapView extends AppCompatActivity {

    private WebView webView;
    private String location, destination, mapLocation, mapDestination;
    private boolean next;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.webView = (WebView) findViewById(R.id.webView);

        setMap();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void setMap() {
        Bundle b = getIntent().getExtras();
        String input1 = "";
        String input2 = "";
        if(b != null) {
            input1 = b.getString("location");
            input2 = b.getString("destination");
        }

        getSupportActionBar().setTitle(input1 + " to " + input2);

        String map1 = getMap(input1);
        String destcp = cpClosestToDest(input2);
        String map2 = getMap(destcp);

        if (map1.equals(map2)) {
            setSingleMap(input1, destcp, map1);
        } else {
            setDoubleMap(input1, destcp, map1, map2);
        }
    }

    private void setSingleMap(String location, String destination, String map) {
        this.webView.loadUrl("file:///android_asset/html/UCLL_Proximus_" + map + ".html");
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.setInitialScale(100);

        final String locationf = location;
        final String destinationf = destination;
        webView.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url){
                webView.evaluateJavascript("goToRooms('" + locationf + "', '" + destinationf + "');", null);
            }
        });
    }

    private void setDoubleMap(String location, String destination, String mapLocation, String mapDestination) {
        this.destination = destination;
        this.mapDestination = mapDestination;

        this.location = location;
        this.mapLocation = mapLocation;

        previousMap();
    }

    public void onNextMapClick(View view) {
        if (next) {
            nextMap();
        } else {
            previousMap();
        }
    }

    private void previousMap() {
        this.webView.loadUrl("file:///android_asset/html/UCLL_Proximus_" + mapLocation + ".html");
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.setInitialScale(100);
        ((Button)findViewById(R.id.nextMapButton)).setVisibility(View.VISIBLE);
        ((Button)findViewById(R.id.nextMapButton)).setText("Next map");

        this.next = true;

        final String locationf = location;
        webView.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url){
                webView.evaluateJavascript("goToRoom('" + locationf + "');", null);
            }
        });
    }

    private void nextMap() {
        Button button = (Button) findViewById(R.id.nextMapButton);
        button.setText("Previous map");

        this.webView.loadUrl("file:///android_asset/html/UCLL_Proximus_" + this.mapDestination + ".html");
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.setInitialScale(100);

        this.next = false;

        final String destinationf = this.destination;
        webView.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url){
                webView.evaluateJavascript("goToRoomsDest('" + destinationf + "');", null);
            }
        });
    }

    private String cpClosestToDest(String destination) {
        Map<String, String> data = new HashMap<>();
        try {
            InputStream is = this.getAssets().open("html/rooms.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String JSONstring = reader.readLine();
            JSONArray jsonArray = new JSONArray(JSONstring);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.getString("name");
                String cp = obj.getString("cp");
                data.put(name, cp);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return data.get(destination);
    }

    private String getMap(String location) {
        Map<String, String> data = new HashMap<>();
        try {
            InputStream is = this.getAssets().open("html/crosspoints.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String JSONstring = reader.readLine();
            JSONArray jsonArray = new JSONArray(JSONstring);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.getString("name");
                String map_location = obj.getString("map_location");
                data.put(name, map_location);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data.get(location);
    }

}
