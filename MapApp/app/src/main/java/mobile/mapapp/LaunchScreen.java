package mobile.mapapp;

import android.content.Intent;
import android.graphics.Typeface;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.TextView;

public class LaunchScreen extends AppCompatActivity  {

    private final int SPLASH_DISPLAY_LENGTH = 2500;
    private Typeface font;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launch_screen);
        this.font = Typeface.createFromAsset(getAssets(), "fonts/Montserrat-Bold.ttf");
        setFont();

        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
                Intent mainIntent = new Intent(LaunchScreen.this,MainActivity.class);
                LaunchScreen.this.startActivity(mainIntent);
                LaunchScreen.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

    private void setFont() {
        ((TextView) findViewById(R.id.appTitle)).setTypeface(font);
        ((TextView) findViewById(R.id.appTitleWelcome)).setTypeface(font);
    }

}
