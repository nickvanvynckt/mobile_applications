package mobile.mapapp;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by nickvanvynckt on 26/01/17.
 */

public class Locator {
    public String getCrosspoint(List<String> wifiList, Map<String, List<String>> mapData){
        return this.calculateCrosspoint(wifiList, mapData);
    }

    private String calculateCrosspoint(List<String> wifiList, Map<String, List<String>> mapData){
        String location = "";
        double percentage = 0.0;

        for(Map.Entry<String, List<String>> entry : mapData.entrySet()){
            List<String> helperList = new ArrayList<>(wifiList);
            helperList.removeAll(entry.getValue());

            double percentageHelp = 1 - (double) helperList.size() / (double) wifiList.size();
            if(percentageHelp >= percentage){
                percentage = percentageHelp;
                location = entry.getKey();
            }
        }
        return location;
    }
}