package mobile.mapapp;

import android.content.Context;
import android.content.Intent;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.view.View;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;


public class MainActivity extends AppCompatActivity {

    private WifiManager wifiManager;
    private List<String> wifiList;
    private int wifiInterval = 2500;
    private Handler wifiHandler;
    private Map<String, List<String>> wifiMapData;
    private Locator locator = new Locator();
    private String location = "";
    private TextView currentCrosspoint;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        this.currentCrosspoint = (TextView) findViewById(R.id.currentCrosspoint);

        wifiMapData = new HashMap<>();
        wifiManager = (WifiManager) getSystemService(Context.WIFI_SERVICE);
        wifiList = new ArrayList<>();
        wifiHandler = new Handler();
        this.checkForWifiPoints();

        AsyncFetchData dataFetcher = new AsyncFetchData();
        dataFetcher.execute();
        try {
            wifiMapData = dataFetcher.get();
        } catch (InterruptedException e) {
            e.printStackTrace();
        } catch (ExecutionException e) {
            e.printStackTrace();
        }

        Spinner dropdown = (Spinner)findViewById(R.id.roomSpinner);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, android.R.layout.simple_list_item_1, getRooms());
        dropdown.setAdapter(adapter);
    }

    Runnable wifiChecker = new Runnable() {
        @Override
        public void run() {
            try {
                wifiManager.startScan();
                wifiList.clear();
                List<ScanResult> results = wifiManager.getScanResults();
                for(ScanResult result: results){
                    wifiList.add(result.BSSID);
                }
                location = locator.getCrosspoint(wifiList, wifiMapData);
                currentCrosspoint.setText(location);
            } finally {
                wifiHandler.postDelayed(wifiChecker, wifiInterval);
            }
        }
    };

    private void checkForWifiPoints() {
        wifiChecker.run();
    }

    private void stopCheckForWifiPoints(){
        wifiHandler.removeCallbacks(wifiChecker);
    }


    public void onCurrentLocationClick(View view) {
        Intent intent = new Intent(MainActivity.this, MapView.class);
        Bundle b = new Bundle();
        b.putString("location", location);
        intent.putExtras(b);
        startActivity(intent);
    }

    public void onGoToRoomClick(View view) {
        Intent intent = new Intent(MainActivity.this, DestMapView.class);
        Bundle b = new Bundle();
        b.putString("location", location);
        b.putString("destination", ((Spinner)findViewById(R.id.roomSpinner)).getSelectedItem().toString());
        intent.putExtras(b);
        startActivity(intent);
    }

    @Override
    protected void onDestroy(){
        super.onDestroy();
        this.stopCheckForWifiPoints();
    }

    private String[] getRooms() {
        ArrayList<String> data = new ArrayList<>();
        try {
            InputStream is = this.getAssets().open("html/rooms.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String JSONstring = reader.readLine();
            JSONArray jsonArray = new JSONArray(JSONstring);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.getString("name");
                data.add(name);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        System.out.println(data.get(0));
        return data.toArray(new String[0]);
    }
}
