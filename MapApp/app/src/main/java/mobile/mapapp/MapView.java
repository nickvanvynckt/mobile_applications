package mobile.mapapp;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by drumb on 26/01/2017.
 */

public class MapView extends AppCompatActivity {

    private WebView webView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.map_view);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        this.webView = (WebView) findViewById(R.id.webView);

        setMap();
    }

    public boolean onOptionsItemSelected(MenuItem item){
        Intent myIntent = new Intent(getApplicationContext(), MainActivity.class);
        startActivityForResult(myIntent, 0);
        return true;
    }

    private void setMap() {
        Bundle b = getIntent().getExtras();
        String input = "";
        if(b != null) {
            input = b.getString("location");
        }

        getSupportActionBar().setTitle(input);

        this.webView.loadUrl("file:///android_asset/html/UCLL_Proximus_" + getMap(input) + ".html");
        this.webView.getSettings().setJavaScriptEnabled(true);
        this.webView.getSettings().setBuiltInZoomControls(true);
        this.webView.setInitialScale(100);

        final String location = input;
        webView.setWebViewClient(new WebViewClient(){
            public void onPageFinished(WebView view, String url){
                webView.evaluateJavascript("goToRoom('" + location + "');", null);
            }
        });
    }

    private String getMap(String location) {
        Map<String, String> data = new HashMap<>();
        try {
            InputStream is = this.getAssets().open("html/crosspoints.json");
            BufferedReader reader = new BufferedReader(new InputStreamReader(is));
            String JSONstring = reader.readLine();
            JSONArray jsonArray = new JSONArray(JSONstring);
            for(int i = 0; i < jsonArray.length(); i++){
                JSONObject obj = jsonArray.getJSONObject(i);
                String name = obj.getString("name");
                String map_location = obj.getString("map_location");
                data.put(name, map_location);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } catch (JSONException e) {
            e.printStackTrace();
        }

        return data.get(location);
    }

}
