package mobile.mapapp;

import java.util.List;
import java.util.Map;

/**
 * Created by nickvanvynckt on 23/01/17.
 */

public interface OnDataLoadListener {
    void onDataIsLoaded(Map<String, List<String>> data);
}
